/*
Zach Coriarty
9/6/18
CSE002
**/

public class Cyclometer{
  
  //main method required for every java program
  public static void main(String[] args){
    
    //variables assigned to seconds for each trip
  	int secsTrip1 = 480;
    int secsTrip2 = 3220;
	  int countsTrip1 = 1561;
	  int countsTrip2 = 9037;
      
    double wheelDiameter = 27.0, //variable for diameter of the wheel
  	       PI = 3.14159, //variable for constant pi
         	feetPerMile = 5280, //variable for amount of feet in each mile
  	      inchesPerFoot = 12, //variable for inches in each foot
  	      secondsPerMinute = 60; //variable for amount of seconds in each minute
	  double distanceTrip1, distanceTrip2,totalDistance; //declared variables for output

    //prints the amount of time the trip took and the number of counts for both trips
    System.out.println("Trip 1 took " +
         (secsTrip1/secondsPerMinute) + " minutes and had " +
            countsTrip1 + " counts.");
	  System.out.println("Trip 2 took " +
         (secsTrip2/secondsPerMinute) + " minutes and had " +
            countsTrip2 + " counts.");
    
    //calculates the total distance of the trip
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    // Above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
  	distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
	  totalDistance = distanceTrip1 + distanceTrip2;
    
   	//Print out the output data.
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	  System.out.println("The total distance was " + totalDistance + " miles");

    
  }//end of main method
}//end of class