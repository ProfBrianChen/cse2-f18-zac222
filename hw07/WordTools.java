/*
Zach Coriarty
10.30.18
CSE002
 */

import java.util.Scanner;

public class WordTools {

    //main method
    public static void main(String[] args) {

        printMenu();//calls the printMenu method

    }

    //prompts the user to enter a sample phrase that they want edited
    public static String sampleText(){
        String input = "";
        Scanner in = new Scanner(System.in);

        System.out.println("Please enter a sample text");
        input = in.nextLine();

        System.out.println("Your output is: " + input);

        return input;
    }
    //method that prints the options menu and designates which method to call
    public static void printMenu() {
        Scanner in = new Scanner(System.in);
        String choice = "";

        while (!choice.equals("q")) {//loops until the user wants to quit

            System.out.println("Menu");
            System.out.println("c - Number of non-whitespace characters");
            System.out.println("w - Number of words");
            System.out.println("f - Find text");
            System.out.println("r - Replace all !'s");
            System.out.println("s - Shorten spaces");
            System.out.println("q - Quit");
            System.out.println("Choose an option: ");

            choice = in.nextLine();//takes the user input for the menu

            if (choice.equals("c")) {
                System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(sampleText()));

            } else if (choice.equals("w")) {
                System.out.println("Number of words: " + getNumOFWords(sampleText()));

            } else if (choice.equals("f")) {
                System.out.println("Please enter a phrase: ");
                String phrase = in.nextLine();
                System.out.println(phrase);
                System.out.println("Please enter a word you want to be found within the inputed phrase: ");
                String word = in.nextLine();

                findText(word, phrase);

            } else if (choice.equals("r")) {
                System.out.println("Please enter a string that you would like edited: ");
                String exPhrase = in.nextLine();
                System.out.println(replaceExclamation(exPhrase));

            } else if (choice.equals("s")) {
                System.out.println("Please enter a phrase: ");
                String shPhrase = in.nextLine();
                System.out.println(shortenSpace("Edited phrase: " + shPhrase));

            } else if (choice.equals("q")) {
                System.out.println();

            } else {
                System.out.println("Not an option, enter an accepted value: ");
            }
        }
    }

    //generates the number of white spaces in the inputed phrase
    public static int getNumOfNonWSCharacters(String a) {

        int counter = 0;
         for (int i = 0; i < a.length(); i++){
             if (a.charAt(i) == ' ') {
                 counter++;
             }
         }
         int totalNonWS = a.length() - counter;

         return totalNonWS;
    }

    //generates the number of words in the inputed phrase
    public static int getNumOFWords(String b) {
        int counter = 0;

        for (int i = 0; i < b.length(); i++) {
            if (b.charAt(i) == ' ') {
                counter++;
            }
        }
        return counter + 1;
    }

    //finds a word in the inputed phrase
    public static int findText(String goal, String given) {

        int counter = 0;
        for (int i = 0; i < given.length() - goal.length(); i++) {
            if (given.substring(i, i + goal.length()).equals(goal)) {
                counter++;
            }
        }
        System.out.println("Your word was found: " + counter + " times.");
        return counter;
    }

    //repleces all exclamations points withing the inputed phrase with a period
    public static String replaceExclamation(String a) {

        String newString = "";
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == '!') {
                newString = a.replace('!', '.');
            }
        }
        return newString;
    }

    //replaces all double spaces with a single space
    public static String shortenSpace(String a) {

        String editedString = "";
        do {
             editedString = a.replace("  ", " ");
             a = editedString;

        } while(a.length() != editedString.length());

        return editedString;
    }
}
