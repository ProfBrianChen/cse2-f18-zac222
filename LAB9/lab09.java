/*
Zach coriarty
11.19.18
lab09
 */

public class lab09 {
    //main method
    public static void main(String[] args) {

        int[] literalArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};//literal array

        //copies of literalArr
        int[] array0 = copy(literalArr);
        int[] array1 = copy(literalArr);
        int[] array2 = copy(literalArr);

        //outputs
        inverter(array0);
        print(inverter2(array1));

        int[] array3 = inverter2(array2);
        print(array3);
    }

    //creates a copy of an inputed array
    public static int[] copy(int[] a) {

        int[] sameLength = new int[a.length];//creates a new array length a

        for (int i = 0; i < a.length; i++) {
            sameLength[i] = a[i];//assigns the same values of a to the sameLength array
        }

        return sameLength;
    }

    //inverts the array b
    public static void inverter(int[] b) {

        for (int i = 0; i < b.length; i++) {
            int place = b[i];//placeholder
            b[i] = b[b.length - i - 1];//assigns the firts element to the value of the last - i
            b[b.length - i- 1] = place;// assigns the first element to the placeholder
            System.out.println(b[i]);//prints it out

        }

    }

    //inverts array c using the same staps as inverter() except it returns an arrray and
    //utilizes the method copy()
    public static int[] inverter2(int[] c) {
        int[] h = copy(c);

        for (int i = 0; i < h.length/2; i++) {
            int place = h[i];
            h[i] = h[h.length - i - 1];
            h[h.length - i - 1] = place;
        }
        return h;
    }

    //prints array d
    public static void print(int[] d) {

        for (int i = 0; i < d.length; i++) {//loops through the length of the array
            System.out.println(d[i] + " ");
        }
    }



}
