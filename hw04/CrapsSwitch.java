/*
Zachary Coriarty
CSE002
9.25.18
**/

import java.util.Scanner;

public class CrapsSwitch{
  
  //main method
  public static void main(String[] args){
    
    Scanner in = new Scanner(System.in);//scanner object
    
    String slang = "";//variable for the output
    int randNum1 = 0;//dice 1
    int randNum2 = 0;//dice 2
    int dice = 11;//switch statement parameter
    
    
    System.out.println("Would you like a randomly cast die or would you like to enter the dice you want evaluated? (enter \"rand\" or \"input\")");
    String userChoice = in.next();//users choice for the dice value
    
    //switch statement that determines whether the user wants the die values genertaed or inputed manually
    switch (userChoice){
      case "rand": randNum1 = (int) (Math.random() * 6) + 1;
                   randNum2 = (int) (Math.random() * 6) + 1;
                   dice = (randNum1 * 10) + randNum2;
            break;
      case "input": System.out.println("Please enter the value of your two dices: ");
                  randNum1 = in.nextInt();
                  randNum2 = in.nextInt();
                  dice = (randNum1 * 10) + randNum2;
            break;
    
      default: System.out.println("Please enter \"rand\" or \"input\"");
  }
    //switch statements that assigns the variable "slang" a slang Craps term based on the die values
    switch(dice){
      case 11: slang = "snake eyes";
        break;
      case 12:
      case 21: slang = "case deuce";
        break;
      case 13:
      case 31: slang = "easy four";
        break;
      case 14:
      case 41: slang = "fever five";
        break;
      case 15:
      case 51: slang = "easy six";
        break;
      case 16:
      case 61: slang = "seven out";
        break;
      case 22: slang = "hard four";
        break;
      case 23:
      case 32: slang = "fever five";
        break;
      case 24:
      case 42: slang = "easy six";
        break;
      case 25:
      case 52: slang = "seven out";
        break;
      case 26:
      case 62: slang = "easy eight";
        break;
      case 33: slang = "hard six";
        break;
      case 34:
      case 43: slang = "seven out";
        break;
      case 35:
      case 53: slang = "easy eight";
        break;
      case 36: 
      case 63: slang = "nine";
        break;
      case 44: slang = "hard eight";
        break;
      case 45:
      case 54: slang = "nine";
        break;
      case 46:
      case 64: slang = "easy ten";
        break;
      case 55: slang = "hard ten";
        break;
      case 56:
      case 65: slang = "yo-leven";
        break;
      case 66: slang = "boxcars";
        break;
      default: System.out.println("invalid input");
        break;
  }
    System.out.println(slang);//prints the slang term
  }//end main method
}//end class