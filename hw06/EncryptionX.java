/*
Zach Coriarty
10.23.18
CSE002
**/

import java.util.Scanner;
public class EncryptionX {

    //main method
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Please enter a value between 1 and 100: ");//prompts the user to input a value between 1 and 100

        //checks the inputed value to be sure it is of type int
        boolean correctInt = in.hasNextInt();

        while(!correctInt){

            System.out.println("Input not accepted. Please enter an integer between 1 and 100: ");
            in.next();
            correctInt = in.hasNextInt();

        }
        int userInput = in.nextInt();

        int counter = userInput - 1;//counter for printing a blank space

        for (int i = 0; i < userInput; i++) {//loop for number of rows

            for (int j = 0; j < userInput; j++) {//loop for outputting a space or *
                if (j == i && j != (userInput / 2)) {
                    System.out.print(" ");//prints a space
                }
                else{
                    System.out.print("*");//prints *
                }
                if (j == counter) {
                    System.out.print(" ");//prints a space
                }
            }
            System.out.println(" ");//prints a space
            counter--;//decrements the counter

        }
    }//end of main method
}//end of class