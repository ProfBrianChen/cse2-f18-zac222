import java.util.Scanner;

    public class hw08{
        public static void main(String[] args) {
            Scanner scan = new Scanner(System.in);
            //suits club, heart, spade or diamond
            String[] suitNames={"C","H","S","D"};
            String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
            String[] cards = new String[52];
            String[] hand = new String[5];
            int numCards = 5;
            int again = 1;
            int index = 51;
            for (int i=0; i<52; i++){
                cards[i]=rankNames[i%13]+suitNames[i/13];
                System.out.print(cards[i]+" ");
            }
            System.out.println();
            shuffle(cards);
            printArray(cards);
            while(again == 1){
                hand = getHand(cards,index,numCards);
                printArray(hand);
                if (numCards > index){
                    shuffle(cards);
                    index = 51;
                }
                index = index - numCards;
                System.out.println("Enter a 1 if you want another hand drawn");
                again = scan.nextInt();
            }
        }

        //prints array
        public static void printArray(String[] list) {

            for (int i = 0; i < list.length; i++) {//loops the length of the array
                System.out.print(list[i] + " ");//print statement
            }

            System.out.println();
        }

        public static void shuffle(String[] list) {//shuffles each element in the array

            String t = "";
            int rand = (int)((Math.random() * 51) + 1);//decalres and initializes variable rand

             System.out.println("Shuffled");
            for (int i = 0; i < 51; i++) {//for loop shuffles the array
                rand = (int)((Math.random() * 51) + 1);

                t = list[i];//variable t is a temporary variable as a place holder
                list[i] = list[rand];
                list[rand] = t;
            }

        }

        //takes a hand from the shuffled deack
        public static String[] getHand(String[] list, int a, int numCards) {

            System.out.println("Hand");
            String[] handArray = new String[numCards];

            if (numCards > a){
                shuffle(list);
                a = 51;
            }

            for (int i = a; i > a - numCards; i--) {//loop that assigns the last five elements of an array into the array "handArray"
                handArray[a - i] = list[i];
            }
            return handArray;//returns the hand
        }


    }
