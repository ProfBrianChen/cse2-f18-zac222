/*
Zach Coriarty
9.20.18
CSE002
**/


public class CardGenerator{
  //main method
  public static void main(String[] args){
    
    int randomCard = (int) (Math.random()* 52) + 1;//generates a random card number
    
    String nameOfSuit = "";//string for the name of the suit
    String identityOfCard = "";//string for the name of the card
    
    //if-statements to determine the suit of the card
    if (randomCard > 0 && randomCard < 14){
      nameOfSuit = "diamonds";
    } else if (randomCard > 13 && randomCard < 27){
      nameOfSuit = "clubs";
    } else if (randomCard > 26 && randomCard < 40){
      nameOfSuit = "hearts";
    } else{
      nameOfSuit = "spades";
    }
    int identity = (int) (Math.random() * 13) + 1;//generates a random number for the variable "identity"
    
    //switch statement to output the identity of a card
    switch (identity) {
        case 1: identityOfCard = "ace";
                break;
        case 2: identityOfCard = "2";
                break;
        case 3: identityOfCard = "3";
                break;
        case 4: identityOfCard = "4";
                break;
        case 5: identityOfCard = "5";
                break;
        case 6: identityOfCard = "6";
                break;
        case 7: identityOfCard = "7";
                break;
        case 8: identityOfCard = "8";
                break;
        case 9: identityOfCard = "9";
                break;
        case 10: identityOfCard = "10";
                break;
        case 11: identityOfCard = "jack";
                break;
        case 12: identityOfCard = "queen";
                break;
        case 13: identityOfCard = "king";
                break;
    }
        System.out.println("You picked the " + identityOfCard + " of " + nameOfSuit);//prints the output
  }
}