/*
Zach Coriarty
CSE002
11.21.18
 */

import java.util.Scanner;
public class RemoveElements {
    public static void main(String[] arg) {
        Scanner scan = new Scanner(System.in);
        int num[] = new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer = "";
        do {
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num, index);
            String out1 = "The output array is ";
            out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num, target);
            String out2 = "The output array is ";
            out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer = scan.next();
        } while (answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]) {
        String out = "{";
        for (int j = 0; j < num.length; j++) {
            if (j > 0) {
                out += ", ";
            }
            out += num[j];
        }
        out += "} ";
        return out;
    }


    //creates a random array with 10 elements
    public static int[] randomInput() {
        int[] randArray = new int[10];//declares an array
        int rand = 0;

        for (int i = 0; i < randArray.length; i++) {
            rand = (int) (Math.random() * 10);//assigns a random number to each element

            randArray[i] = rand;
        }
        return randArray;//returns the new array
    }

    //deletes the value in position pos
    public static int[] delete(int[] list, int pos) {
        int[] newArray = new int[list.length - 1];//declares an array

        for (int i = 0; i < newArray.length; i++) {
            if (list[i] != list[pos]) {//creates the new aray minue the desired index
                newArray[i] = list[i];
            }
            else{continue;}
        }
        return newArray;//returns teh new array
    }

    //removes every instance of the target
    public static int[] remove(int[] list, int target) {

        int[] newArray = new int[list.length - 1];//declares an array

        for (int i = 0; i < newArray.length; i++) {
            if (list[i] != target) {//
                newArray[i] = list[i];

            }
            else{continue;}
        }
        return newArray;//returns the new array
    }
}
