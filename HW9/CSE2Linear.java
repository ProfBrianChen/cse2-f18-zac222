/*
Zach Coriarty
CSE002
11.21.18
 */

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {

    //main method
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);//creates a scanner object
        int[] inputArr = new int[15];

        //place holder variables
        int counter = 0;
        int place = 0;
        int hold = 0;

        System.out.println("Please enter 15 integer values: ");//prmpts user to enter values
        boolean correctInt = in.hasNextInt();


        for (int i = 0; i < 15; i++) {
            //ensures the values entered oare of type int
            while (!correctInt) {
                in.next();
                System.out.println("Input not accepted. Please enter the number of your course: ");//prmpts user to enter values
                correctInt = in.hasNextInt();
            }
            int temp = in.nextInt();//takes value

            if (temp < 0 || temp > 100) {//makes sure each value is between 1 and 100
                System.out.println("Please enter a value between 0 and 100");//prmpts user to enter values
                counter++;

            } else {
                inputArr[i] = temp;
            }
            if (temp < inputArr[i - place]) {
                System.out.println("Please enter a value that is greater than or equal to the previous value");//prmpts user to enter values
            }
            place = 1;


        }
        //prints array
        for (int i = 0; i < inputArr.length; i++) {
            System.out.print(inputArr[i] + " ");
        }
        System.out.println();

        System.out.println("Please enter a value to be searched for: ");//prmpts user to enter values
        int search = in.nextInt();
        binarySearch(inputArr, search);

        System.out.println();
        System.out.println("Please enter a grade to be searched for: ");//prmpts user to enter values
        int linearIn = in.nextInt();

        linearSearch(randomize(inputArr), linearIn);//calls public methods

    }

    //uses the binary search method on array arr
    public static int binarySearch(int arr[], int a) {

        int low = 0;//low index in array
        int high = arr.length - 1;//high index in array
        int iterations = 0;//counts the number of iterations

        while (high >= low) {
            int mid = (low + high) / 2;
            if (a < arr[mid]) {
                high = mid - 1;
                iterations++;
            } else if (a == arr[mid]) {
                iterations++;
                System.out.println("The grade was found with " + iterations + " iterations");//prints the output
                return mid;

            } else {
                low = mid + 1;
                iterations++;
            }
        }
        System.out.println("The grade was not found");
        return -1;
    }

    //searches the array using a linear search
    public static void linearSearch(int[] arr, int a) {

        int iterations = 0;//counts the number of iterations

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == a) {

                iterations++;
                System.out.println("The grade is " + arr[i] + " and was found with " + iterations + " iterations.");
            } else {
                iterations++;

            }
        }
    }

    //changes the order of the array
    public static int[] randomize(int[] arr) {

        int hold = 0;//place holder
        for (int i = 0; i < arr.length; i++) {
            int rand = (int) (Math.random() * 15);

            hold = arr[i];//assigns index i to the holder
            arr[i] = arr[rand];//changes the value at index i
            arr[rand] = hold;//switches teh holder value with the changed value
        }

        //prints the array
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        return arr;//returns the array
    }
}
