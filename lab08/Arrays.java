public class Arrays {

    //main method
    public static void main(String[] in){

        int[] arrayOne = new int[100];//decalres and initializes the first array
        int[] arrayTwo = new int[99];//decalres and initializes the second array
        int j = 0;
        int counter = 0;//counts the number of occurences of a number


        //assigns a random number to the first array
        for (int i = 0; i < arrayOne.length; i++){
            arrayOne[i] = (int) (Math.random() * 100);

        }
//        for (int i = 0; 0 < arrayOne.length; i++) {
//            System.out.println(arrayOne[i] + " ");;// prints each element of the array
//        }

            //nested for-loop that counts the number of times an int occurs in arrayOne and assigns that value to arrayTwo
            for (j = 0; j < arrayOne.length; j++) {
                for (int k = 0; k < arrayOne.length; k++) {
                    if (arrayOne[k] == j) {
                        counter++;
                        arrayTwo[j] = counter;
                    }
                }
                System.out.println(j + " occurs " + counter + " times");//prints the output
                counter = 0;//resets the counter

            }
        }
    }
