public class Arithmetic{
  
  //main method
  public static void main(String[] args){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //declare all variables for calculating prices
    double totalPantsPrice;
    double totalSweatshirtsPrice;
    double totalBeltsPrice;
    
    //declare all variables for tax
    double pantsTax;
    double sweatshirtTax;
    double beltsTax;
    
    //declare variable for overall price value
    double overallPrice;
    
    //declare variable for overall tax value
    double overallTax;
    
    //declare variables for price with tax
    double totalPriceWithTax;
    
    totalPantsPrice = (numPants * pantsPrice);//total price of pants
    totalSweatshirtsPrice = (numShirts * shirtPrice);//total price of sweatshirts
    totalBeltsPrice = (numBelts * beltCost);//total price of belts
    
    pantsTax = (totalPantsPrice * paSalesTax);//tax on pants
    sweatshirtTax = (totalSweatshirtsPrice * paSalesTax);//tax on sweatshirts
    beltsTax = (totalBeltsPrice * paSalesTax);//tax on belts
    
    overallPrice = (totalBeltsPrice + totalSweatshirtsPrice + totalPantsPrice);//total price before tax
    overallTax = (pantsTax + sweatshirtTax + beltsTax);//total tax
    
    totalPriceWithTax = (overallTax + overallPrice);//final cost, price and tax included
    
    /*
    prints the cost of each item, the tax for each item, 
    the total cost before tax, the total tax for the items, 
    and the final price including tax.
    **/
    System.out.printf("%s%.2f%n", "The total cost of pants before tax is: $", totalPantsPrice);
    System.out.printf("%s%.2f%n", "The total cost of sweatshirts before tax is: $", totalSweatshirtsPrice);
    System.out.printf("%s%.2f%n", "The total cost of belts before tax is: $", totalBeltsPrice);
    System.out.printf("%s%.2f%n", "The total cost of pants tax is: $", pantsTax);
    System.out.printf("%s%.2f%n", "The total cost of sweatshirt tax is: $", sweatshirtTax);
    System.out.printf("%s%.2f%n", "The total cost of belts tax is: $", beltsTax);
    System.out.printf("%s%.2f%n", "The total cost of all items before tax is: $", overallPrice);
    System.out.printf("%s%.2f%n", "The total cost of tax is: $", overallTax);
    System.out.printf("%s%.2f%n", "The total cost of all items with tax is: $", totalPriceWithTax);

  }//end main method
}//end class