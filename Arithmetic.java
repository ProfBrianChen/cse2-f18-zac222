public class Arithmetic{
  
  //main method
  public static void main(String[] args){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //declare all variables for calculating prices
    double totalPantsPrice;
    double totalSweatshirtsPrice;
    double totalBeltsPrice;
    
    //declare all variables for tax
    double pantsTax;
    double sweatshirtTax;
    double beltsTax;
    
    //declare variable for overall price value
    double overallPrice;
    
    //declare variable for overall tax value
    double overallTax;
    
    //declare variables for price with tax
    double totalPriceWithTax;
    
    totalPantsPrice = numPants * pantsPrice;//total price of pants
    totalSweatshirtsPrice = numShirts * shirtPrice;//total price of sweatshirts
    totalBeltsPrice = numbelts * beltCost;//total price of belts
    
    pantsTax = totalPantsPrice * paSalesTax;//tax on pants
    sweatshirtTax = totalSweatshirtsPrice * paSalesTax;//tax on sweatshirts
    beltsTax = totalBeltsPrice * paSalesTax;//tax on belts
    
    overallPrice = totalBeltsPrice + totalSweatshirtsPrice + totalPantsPrice;//total price before tax
    overallTax = pantsTax + sweatshirtTax + beltsTax;//total taxß
    
    totalPriceWithTax = overallTax + overallPrice;//final cost, price and tax included
    
    System.out.printf("%.3f", "The price of pants before tax is: " + totalPantsPrice);

  }
}