/*
Zach Criarty
CSE002
11.3.18
 */

import java.util.Scanner;

public class HW10 {

    //main method
    public static void main(String[] args) {
        int[][] grid = { {1, 2, 3},
                       {4, 5, 6},
                       {7, 8, 9} };//initializes the array grid
        ticTac(grid);//calls the method ticTac

    }


    public static void ticTac(int[][] a) {
        Scanner in = new Scanner(System.in);

        for (int i = 0; i < 9; i++) {//loops per each move

            print(a);
            System.out.println("Please enter a value for the location of your move: ");//prompts the user to enter a value
            boolean correctInt = in.hasNextInt();


            while(!correctInt){//checks to make shure the input is an int
                in.next();
                System.out.println("Please enter an integer: ");
                correctInt = in.hasNextInt();

            }
            int x = in.nextInt();
            //checks to make sure the value is in range
            if(x <= 0 || x >= 10){
                System.out.println("Please enter a value between 1 and 9");
                i--;
                continue;
             //checks to make sure the value has not yet been taken
            }else if (a[(x-1)/3][(x - 1) % 3] != x){
                System.out.println("Please enter a value that has not already been taken");
                i--;
                continue;
            }

            a[(x-1)/3][(x - 1) % 3] = -1;//hold place of X
            winner(a);//calls winner
            print(a);//calls print

            System.out.println("Please enter a value for the location of your move: ");//prompts the user to enter a value
            correctInt = in.hasNextInt();


            while(!correctInt){//checks to make shure the input is an int
                in.next();
                System.out.println("Please enter an integer: ");
                correctInt = in.hasNextInt();

            }
            //checks to make sure the value is in range
            int o = in.nextInt();
            if(o <= 0 || o >= 10){
                System.out.println("Please enter a value between 1 and 9");
                i--;
                continue;

                //checks to make sure the value has not yet been taken
            }else if (a[(o-1)/3][(o - 1) % 3] != o){
                System.out.println("Please enter a value that has not already been taken");
                i--;
                continue;
            }

            a[(o-1)/3][(o - 1) % 3] = -2;//holds the place of O
            winner(a);//calls winner


        }
        System.out.println("There was no winner...");
    }

    //prints the array
    public static void print(int[][] a){
        for (int j = 0; j < a.length; j++) {
            for (int k = 0; k < a[j].length; k++) {
                if(a[j][k] == -1){//fills the array with X
                    System.out.print("x" + " ");
                }

                else if(a[j][k] == -2){//fills the array with O
                    System.out.print("o" + " ");
                }
                else {
                    System.out.print(a[j][k] + " ");
                }
            }
            System.out.println();
        }
    }

    //determines the winner
    public static void winner(int[][] a) {

        for (int j = 0; j < a.length; j++) {
                if(a[j][0] == a[j][1] && a[j][1] == a[j][2]){//checks for the horizontal win
                    if (a[j][0] == -1) {
                        System.out.println("Congratulations! X has won!");
                        System.exit(0);

                    }
                    if (a[j][0] == -2) {
                        System.out.println("Congratulations! O has won!");
                        System.exit(0);

                    }
                }
                if(a[0][j] == a[1][j] && a[1][j] == a[2][j]){//checks for the vertical win
                    if (a[0][j] == -1) {
                        System.out.println("Congratulations! X has won!");
                        System.exit(0);

                    }
                    if (a[0][j] == -2) {
                        System.out.println("Congratulations! O has won!");
                        System.exit(0);

                    }
                }

            }
        if(a[0][0] == a[1][1] && a[1][1] == a[2][2] || a[0][2] == a[1][1] && a[1][1] == a[2][0]){//checks for the diagonal win
            if (a[0][0] == -1 || a[0][2] == -1) {
                System.out.println("Congratulations! X has won!");
                System.exit(0);
            }
            if (a[0][0] == -2 || a[0][2] == -2) {
                System.out.println("Congratulations! O has won!");
                System.exit(0);

            }

        }


    }

}

