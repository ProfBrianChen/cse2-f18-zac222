/*
Zach Coriarty
CSE002
hw03
9.13.18
Write a program that asks the user for doubles
that represent the number of acres of land affected
by hurricane precipitation and how many inches
of rain were dropped on average.
**/

import java.util.Scanner;

public class Convert{
  
  //main method
  public static void main(String[] args){
    
    Scanner in = new Scanner(System.in);//creates the Scanner object "in"
      
    System.out.println("Please enter the number of acres of land that were infected: ");//prompts the user to enter the number of acres of land affected by rain 
    
    double numOfAcres = in.nextDouble();//accepts a double by the user for acres
    
    System.out.println("Please enter the amount of rain that was dropped, on average: ");//prompts the user to enter the average amount of rain dropped
    
    double inchesOfWater = in.nextDouble();//accepts a double by the user fo rinches of water
    
    int gallons = 27154;//the number of gallons per one inch of rain on one acre
    
    double convertToCubicMiles = gallons / (numOfAcres * inchesOfWater);//converts the inputed values to cubic miles
    
    System.out.printf("%.8f%s%n", convertToCubicMiles,  " cubic miles");//prints the number of cubic miles to 8 decimal places
  }//end of method
}//end of class