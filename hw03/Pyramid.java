/*
Zachary Coriarty
CSE002
hw03
**/

import java.util.Scanner;

public class Pyramid{
  
  //main method
  public static void main(String[] args){
    
    Scanner in = new Scanner(System.in);//creates the Scanner object "in"
    
    System.out.println("Please enter the height of the pyramid: ");//asks the user to enter the height of the triangle
    
    double height = in.nextDouble();//accepts a double by the user for height
    
    System.out.println("Please enter the length of the pyramid: ");//asks the user to enter the length of the triangle
    
    double length = in.nextDouble();//accepts a double by the user for length
    
    System.out.println("Please enter the width of the pyramid: ");//asks the user to enter the width of the triangle
    
    double width = in.nextDouble();//accepts a double by the user for width
    
    double totalVolume = (length * width * height) / 3;//calculates the volume based on the inputed values
    
    System.out.printf("%s%.3f%s%n", "The volume of the pyramid is: ", totalVolume, " cubic units");//prints the total volume
    
  }//end of main method
}//end of class