/*
Zach Coriarty
CSE002
10.25.18

Note to grader: The random methods sometimes don't generate a word and the TA's at lab couldn't figure out why
 **/

import java.util.Random;
import java.util.Scanner;

public class Methods {

    //main method
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int again = 1;

        while (again == 1) {//loops in parameter is true

            for (int i = 1; i <= 1; i++) {//loops once per while loop
                System.out.println("The " + adjective() + subject() + verb() + "at the " + object());//prints primary sentence
            }
            System.out.println("Enter \"1\" if you want to generate another sentence, if not, enter \"2\"");//asks user if another sentence should be printed
            again = in.nextInt();
        }
        //prompts user for whether a paragraph should be generated, if yes, it calls the story method
        System.out.println("Would you like a full story? (\"1\" for yes or \"2\" for no)");
        int userInput = in.nextInt();

        if (userInput == 1) {
            story();
        } else {
            System.out.println("Your loss.");
        }
    }//end of main method

    public static void story(){//generates a paragraph based on the below methods

        System.out.println("The " + adjective() + subject() + verb() + "at the " + object());
        System.out.println("The " + subject() + verb() + "but is still mad at the " + object());
        System.out.println("Having " + verb() + "very far," + " the " + object() + "still fought back!");
        System.out.println("Luckily, the " + subject() + "won!");
    }

    public static String adjective(){//generates a random adjective
        Random randNum = new Random();

        int randomInt = randNum.nextInt((9) + 1);

        String randAdj = "";
        switch (randomInt) {
            case 1: randAdj = "blue ";
                break;
            case 2: randAdj = "orange ";
                break;
            case 3: randAdj = "green ";
                break;
            case 4: randAdj = "cool ";
                break;
            case 5: randAdj = "extravagant ";
                break;
            case 6: randAdj = "dumb ";
                break;
            case 7: randAdj = "smart ";
                break;
            case 8: randAdj = "funny ";
                break;
            case 9: randAdj = "boring ";
                break;
        }
        return randAdj;
    }

    public static String subject(){//generates a random subject
        Random randNum = new Random();

        int randomInt = randNum.nextInt((9) + 1);

        String randSub = "";
        switch (randomInt) {
            case 1: randSub = "dog ";
                break;
            case 2: randSub = "clam ";
                break;
            case 3: randSub = "baby ";
                break;
            case 4: randSub = "cat ";
                break;
            case 5: randSub = "lion ";
                break;
            case 6: randSub = "president ";
                break;
            case 7: randSub = "king ";
                break;
            case 8: randSub = "dad ";
                break;
            case 9: randSub = "moose ";
                break;
        }
        return randSub;
    }

    public static String verb(){//generates a random verb
        Random randNum = new Random();

        int randomInt = randNum.nextInt((9) + 1);

        String randVerb = "";
        switch (randomInt) {
            case 1: randVerb = "ran ";
                break;
            case 2: randVerb = "jumped ";
                break;
            case 3: randVerb = "sprinted ";
                break;
            case 4: randVerb = "sprung ";
                break;
            case 5: randVerb = "swam ";
                break;
            case 6: randVerb = "climbed ";
                break;
            case 7: randVerb = "lunged ";
                break;
            case 8: randVerb = "crawled ";
                break;
            case 9: randVerb = "hamspringed ";
                break;
        }
        return randVerb;
    }

    public static String object(){//generates a random object
        Random randNum = new Random();

        int randomInt = randNum.nextInt((9) + 1);

        String randObj = "";
        switch (randomInt) {
            case 1: randObj = "car ";
                break;
            case 2: randObj = "man ";
                break;
            case 3: randObj = "eiffel tower ";
                break;
            case 4: randObj = "train ";
                break;
            case 5: randObj = "building ";
                break;
            case 6: randObj = "movie star ";
                break;
            case 7: randObj = "banana man ";
                break;
            case 8: randObj = "friend ";
                break;
            case 9: randObj = "blue car ";
                break;
        }
        return randObj;
    }
}//end of class
