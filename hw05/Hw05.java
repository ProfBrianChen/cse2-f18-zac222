import java.util.Scanner;
public class Hw05 {

    //main method
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);//creates scanner object
        int card1 = 0;//creates the first card in the hand
        int card2 = 0;//creates the second card in the hand
        int card3 = 0;//creates the third card in the hand
        int card4 = 0;//creates the fourth card in the hand
        int card5 = 0;//creates the fifth card in the hand
        int faceValue1 = 0;//creates the number value of the first card
        int faceValue2 = 0;//creates the number value of the second card
        int faceValue3 = 0;//creates the number value of the third card
        int faceValue4 = 0;//creates the number value of the fourth card
        int faceValue5 = 0;//creates the number value of the fifth card
        int fourCount = 0;//counts the number of hands that have a four-of-a-kind
        int fullHouse = 0;//counts the number of hands that have a full house
        int threeCount = 0;//counts the number of hands that have a three-of-a-kind
        int twoCount = 0;//counts the number of hands that have a two pair
        int pairCount = 0;//counts the number of hands that have a pair



        System.out.println("How many times would you like to generate a hand?");//asks user how many times they want a new hand generated

        //ensures the user enters an integer
        boolean correctInt = in.hasNextInt();

        while (!correctInt) {
            System.out.println("Please enter an integer: ");
            in.next();
            correctInt = in.hasNextInt();
        }
        double userInput = in.nextInt();

        int identity = 0;


        while (identity < userInput) {

            //determines the face value of card1
            card1 = ((int) (Math.random() * 52) + 1);

            faceValue1 = card1 % 13;

            //determines the face value of card2 and checks to make sure it does not have the same value as card1
            do {
                card2 = ((int) (Math.random() * 52) + 1);


            } while ( card1 == card2);

            faceValue2 = card2 % 13;

            card3 = ((int) (Math.random() * 52) + 1);

            //determines the face value of card3 and checks to make sure it does not have the same value as card2
            do {
                card3 = ((int) (Math.random() * 52) + 1);
            } while ( card1 == card3 || card2 == card3);

            faceValue3 = card3 % 13;

            card4 = ((int) (Math.random() * 52) + 1);

            //determines the face value of card4 and checks to make sure it does not have the same value as card3
            do {
                card4 = ((int) (Math.random() * 52) + 1);
            } while ( card1 == card4 || card2 == card4 || card3 == card4);

            faceValue4 = card4 % 13;

            card5 = ((int) (Math.random() * 52) + 1);

            //determines the face value of card5 and checks to make sure it does not have the same value as card4
            do {
                card5 = ((int) (Math.random() * 52) + 1);
            } while ( card1 == card5 || card2 == card5 || card3 == card5 || card4 == card5);

            faceValue5 = card5 % 13;

            //System.out.println("Five face values:" + faceValue1 + ", " + faceValue2 +", " +faceValue3+ ", " + faceValue4 + ", " + faceValue5);

            identity++;//runs the while loop for the amount of times the user prompted

            //checks for four-of-a-kind in the hand and increments the counter
            if ( faceValue1 == faceValue2 && faceValue2 == faceValue3 && faceValue3 == faceValue4 ||
            faceValue2 == faceValue3 && faceValue3 == faceValue4 && faceValue4 == faceValue5 ||
            faceValue3 == faceValue4 && faceValue4 == faceValue5 && faceValue5 == faceValue1 ||
            faceValue4 == faceValue5 && faceValue5 == faceValue1 && faceValue1 == faceValue2 ||
            faceValue5 == faceValue1 && faceValue1 == faceValue2 && faceValue2 == faceValue3
            ){
                fourCount++;
            }

            //checks for three-of-a-kind and full house in the hand and increments the counter
            else if (faceValue1 == faceValue2 && faceValue2 == faceValue3){

                threeCount++;

                if (faceValue4 == faceValue5)

                    fullHouse++;

            } else if (faceValue2 == faceValue3 && faceValue3 == faceValue4) {

                threeCount++;

            } if (faceValue5 == faceValue1) {

                    fullHouse++;
            } else if (faceValue3 == faceValue4 && faceValue4 == faceValue5) {

                    threeCount++;
                } if (faceValue1 == faceValue2) {
                        fullHouse++;


            } else if (faceValue4 == faceValue5 && faceValue5 == faceValue1) {
                    threeCount++;

                } if (faceValue2 == faceValue3) {
                        fullHouse++;
            } else if (faceValue5 == faceValue1 && faceValue1 == faceValue2) {
                    threeCount++;

            }
                if (faceValue3 == faceValue4) {
                    fullHouse++;

                    //checks for a two pair in the hand and increments the counter
                } else if (faceValue1 == faceValue2 && faceValue3 == faceValue4 ||
                           faceValue2 == faceValue3 && faceValue4 == faceValue5 ||
                           faceValue3 == faceValue4 && faceValue5 == faceValue1 ||
                           faceValue4 == faceValue5 && faceValue1 == faceValue2 ) {
                    twoCount++;

                    //checks for a pair in the hand and increments the counter
                } else if (faceValue1 == faceValue2 || faceValue2 == faceValue3 || faceValue3 == faceValue4 || faceValue4 == faceValue5) {
                    pairCount++;
                }

         }

         //prints the probability for each
        System.out.printf("%s%.3f%n", "Number of loops: ", userInput);
        System.out.printf("%s%.3f%n", "Probability of four-of-kind: ", (fourCount / userInput));
        System.out.printf("%s%.3f%n", "Probability of three-of-kind: ", (threeCount / userInput));
        System.out.printf("%s%.3f%n", "Probability of two pair: ", (twoCount / userInput));
        System.out.printf("%s%.3f%n", "Probability of one-pair: ", (pairCount / userInput));


            }
        }




