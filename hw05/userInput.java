import java.util.Scanner;
public class userInput {
    //main method
    public static void main(String[] args){

        Scanner in = new Scanner(System.in);

        //while loop that asks for the name of your course
        System.out.println("Please enter the number of your course: ");
        boolean correctInt = in.hasNextInt();


        while(!correctInt){

            System.out.println("Input not accepted. Please enter the number of your course: ");
            in.next();
            correctInt = in.hasNextInt();

        }

        int courseNumber = in.nextInt();

        //while loop that asks for the department name
        System.out.println("Please enter the department name of your course: ");
        boolean correctString = in.hasNext();

        while(!correctString){

            System.out.println("Input not accepted. Please enter name of your course's department: ");
            in.next();
            correctString = in.hasNext();

        }

        String courseDept = in.next();

        //while loop that asks for the number of meeting times
        System.out.println("Please enter the number of meeting times: ");
        boolean correctInt2 = in.hasNextInt();

        while(!correctInt2){

            System.out.println("Input not accepted. Please enter the number of meeting times: ");
            in.next();
            correctInt2 = in.hasNextInt();

        }

        int numMeet = in.nextInt();

        //while loop that asks for the start of class time
        System.out.println("Please enter the class start time: ");
        boolean correctInt3 = in.hasNextInt();

        while(!correctInt3){

            System.out.println("Input not accepted. Please enter the class meeting time(10:30 = 1030): ");
            in.next();
            correctInt3 = in.hasNextInt();

        }

        int classTime = in.nextInt();

        //while loop that asks for the instructors name
        System.out.println("Please enter your instructor's name: ");
        boolean correctString2 = in.hasNext();

        while(!correctString2) {

            System.out.println("Input not accepted. Please enter the name of your instructor: ");
            in.next();
            correctString2 = in.hasNext();

        }

        String instName = in.next();

        //while loop that asks for the number of students
        System.out.println("Please enter the number of students: ");
        boolean correctInt4 = in.hasNextInt();

        while(!correctInt4){

            System.out.println("Input not accepted. Please enter the number of students: ");
            in.next();
            correctInt4 = in.hasNextInt();

        }

        int numStudents = in.nextInt();
    }//end of main method
}//end of class